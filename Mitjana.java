import java.util.Scanner;

public class Mitjana {

	public static void main(String[] args) {
		
		float primernombre, segonnombre, tercernombre, sumadenombres, mitjana; //declarem variables
		
		Scanner sc = new Scanner(System.in); //creem objecte scanner

		//demanem tres nombres enters
		System.out.print("Escriu el primer nombre: ");
		primernombre = sc.nextFloat();
		
		System.out.print("Escriu el segon nombre: ");
		segonnombre = sc.nextFloat();
		
		System.out.print("Escriu el tercer nombre: ");
		tercernombre = sc.nextFloat();
		
		//operem
		sumadenombres = primernombre+segonnombre+tercernombre;
		mitjana = sumadenombres/3; //en aquest cas tres, ja que son tres nombres.
		
		//mostrem resultats
		System.out.println("La mitjana �s "+mitjana);
		
	}

}
