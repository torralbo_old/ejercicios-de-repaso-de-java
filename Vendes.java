import java.util.Scanner;

public class Vendes {

	public static void main(String[] args) {
		
		//declarem constants
		final double SALARI = 800.00;
		final double COMISSIO = 170.00;
		//declarem variables
		int automobilsvenuts;
		double nombrevendes;
		
		Scanner sc = new Scanner(System.in); //creem objecte scanner
		
		//agafem informaci� per teclat
		System.out.print("Nombre d'autom�bils venuts: ");
		automobilsvenuts = sc.nextInt();
		System.out.print("Nombre de la suma de vendes: ");
		nombrevendes = sc.nextDouble();
		
		/*declarem variable de la operaci� del salari final
		 * 
		 * en aquest cas el salari m�s la comissi� per cada autom�bil venut i el 5% del nombre de les vendes.
		 */
		double salarifinal = (SALARI+(COMISSIO*automobilsvenuts)+(nombrevendes*5/100.0));
		
		//imprimim resultats
        System.out.printf ("\nEl salari del treballador d'aquest m�s �s de "+"%.2f"+" \u20AC", salarifinal);

		
	}

}
