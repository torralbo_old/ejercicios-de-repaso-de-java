import java.util.Scanner;
public class OperacionsBasiques {

	public static void main(String[] args) {
		
		int primernombre, segonnombre, sumar, restar, multiplicar, dividir;
		
		Scanner sc = new Scanner(System.in); //creem objecte scanner
		
		//demanem dos nombres enters (INT)
		System.out.print("Escriu el primer nombre enter: ");
	    primernombre = sc.nextInt();
		
		System.out.print("Escriu el segon nombre enter: ");
		segonnombre = sc.nextInt();
		
		//operem
		sumar = primernombre+segonnombre;
		restar = primernombre-segonnombre;
		multiplicar = primernombre*segonnombre;
		dividir = primernombre/segonnombre;
		
		//mostrem resultats de les operacions
		System.out.println("\nLa suma de "+primernombre+" i "+segonnombre+" �s: "+sumar);
		System.out.println("La resta de "+primernombre+" i "+segonnombre+" �s: "+restar);
		System.out.println("La multiplicaci� de "+primernombre+" i "+segonnombre+" �s: "+multiplicar);
		System.out.println("La divisi� de "+primernombre+" i "+segonnombre+" �s: "+dividir);		
		
	}

}
